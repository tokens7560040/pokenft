

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract PokeNFT is ERC721, Ownable {
  
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIdCounter;
    uint256 public mintRate = 0.01 ether;
    uint public MAX_SUPPLY = 10000;
    uint256 private _totalSupply;

     mapping(uint256 => string) private _tokenURIs;

     event TokenMinted(address indexed to, uint256 indexed tokenId, string tokenURI);
   
    constructor() ERC721("PokeNFT", "PNFT") {
        require(MAX_SUPPLY > 0, "MAX_SUPPLY must be greater than 0");
    }

    function _baseURI() internal pure override returns (string memory) {
        return "https://api.pokenft.com/tokens/";
    }

     function setTokenURI(uint256 tokenId, string memory tokenURI) external onlyOwner {
        require(_exists(tokenId), "URI set for nonexistent token");
        _tokenURIs[tokenId] = tokenURI;
    }

    function getTokenURI(uint256 tokenId) public view returns (string memory) {
        require(_exists(tokenId), "URI query for nonexistent token");
        return _tokenURIs[tokenId];
    }

    function safeMint(address to, string memory tokenURI) public payable {
        require( _totalSupply < MAX_SUPPLY, "Can't mint more");
        require(msg.value >= mintRate, "Not enough ether sent");
        _tokenIdCounter.increment();
        uint256 newTokenId = _tokenIdCounter.current();
        _safeMint(to, newTokenId, "");
        // Set the token URI
        _tokenURIs[newTokenId] = tokenURI;


        emit TokenMinted(to, newTokenId, tokenURI);
    }

    // Override the _beforeTokenTransfer function from ERC721 or ERC721Enumerable
   

    function supportsInterface(bytes4 interfaceId) public view override(ERC721) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    function withdraw() public onlyOwner {
        require(address(this).balance > 0, "Balance is 0");
        payable(owner()).transfer(address(this).balance);
    }
}





